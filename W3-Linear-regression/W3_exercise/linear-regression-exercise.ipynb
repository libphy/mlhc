{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import scipy as sp\n",
    "import scipy.stats as stats\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "# Set color map to have light blue background\n",
    "sns.set()\n",
    "import statsmodels.formula.api as smf\n",
    "import statsmodels.api as sm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "N.B.: I recommend that you use the `statsmodel` library to do the regression analysis as opposed to *e.g.* `sklearn`. The `sklearn` library is great for advanced topics, but it's easier to get lost in a sea of details and it's not needed for these problems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Body Mass Index Model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this problem, you will first clean a data set and create a model to estimate body fat based on the common BMI measure. Then, you will use the **forward stepwise selection** and **backware stepwise selection** methods to create more accurate predictors for body fat.\n",
    "\n",
    "The body density dataset in file `bodyfat` includes the following 15 variables listed from left to right:\n",
    "* Density : Density determined from underwater weighing\n",
    "* Fat : Percent body fat from Siri’s (1956) equation\n",
    "* Age : Age (years)\n",
    "* Weight : Weight (kg)\n",
    "* Height : Height (cm)\n",
    "* Neck : Neck circumference (cm)\n",
    "* Chest: Chest circumference (cm)\n",
    "* Abdomen : Abdomen circumference (cm)\n",
    "* Hip : Hip circumference (cm)\n",
    "* Thigh : Thigh circumference (cm)\n",
    "* Knee : Knee circumference (cm)\n",
    "* Ankle : Ankle circumference (cm)\n",
    "* Biceps : Biceps (extended) circumference (cm)\n",
    "* Forearm : Forearm circumference (cm)\n",
    "* Wrist : Wrist circumference (cm)\n",
    "\n",
    "The `Density` column is the \"gold standard\" -- it is a measure of body density obtained by dunking people in water and measuring the displacement. The `Fat` column is a prediction using another statistical model. The body mass index (BMI) is [calculated as Kg/m^2](https://en.wikipedia.org/wiki/Body_mass_index) and is used to classify people into different weight categories with a [BMI over 30 being 'obese'](https://www.medicalnewstoday.com/info/obesity). You will find that BMI is a poor predictor of the `Density` information it purports to predict. You will try to find better models using measurements and regression.\n",
    "\n",
    "Unfortunately for us, the dataset we have has imperial units for weight and height, so we will convert those to metric and then calculate the BMI and plot the KDE of the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fat = pd.read_csv('bodyfat.csv')\n",
    "fat = fat.drop('Unnamed: 0', axis=1)\n",
    "fat.Weight = fat.Weight * 0.453592 # Convert to Kg\n",
    "fat.Height = fat.Height * 0.0254 # convert inches to m\n",
    "fat['BMI'] = fat.Weight / (fat.Height**2)\n",
    "fat.BMI.plot.kde();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### a.\n",
    "The BMI has at least one outlier since it's unlikely anyone has a BMI of 165, even [Arnold Schwarzenegger](http://www.health.com/health/gallery/0,,20460621,00.html).\n",
    "\n",
    "Form a new table `cfat` (cleaned fat) that removes any rows with a BMI greater than 40 and calculate the regression model predicting the `Density` from the `BMI`. Display the summary of the regression model. You should achieve an $R^2$ of at least 0.53."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-6250a219bbe962cd",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### b. \n",
    "Plot your regression model against the BMI measurement, properly labeling the scatterplot axes and showing the regression line. In subsequent models, you will not be able to plot the Density *vs* your predictors because you will have too many predictors, but it's useful to visually understand the relationship between the BMI predictor and the `Density` because you should find that the regression line goes through the data but there is too much variability in the data to achieve a good $R^2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-7ca3b6e7f3242bb2",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constructing Better Predictors for Bodyfat "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " The `BMI` model uses easy-to-measure predictors, but has a poor $R^2 \\sim 0.54$. We will use structured subset selection methods from ISLR Chapter 6.1 to derive two better predictors. That chapter covers *best subset*, *forward stepwise* and *backware stepwise* selection. I have implemented the *best subset* selection which searches across all combinations of $1, 2, \\ldots, p$ predictors and selects the best predictor based on the $\\textit{adjusted}~R^2$ metric. This method involved analyzing $2^{13} = 8192$ regression models (programming and computers for the win). The resulting $\\textit{adjusted}~R^2$ plot is shown below:\n",
    " \n",
    " <img src='fat-best-rsqadj.png'>\n",
    " \n",
    "In this plot, `test_fat` and `train_fat` datasets each containing 200 randomly selected samples were derived from the `cfat` dataset using `np.random.choice` over the `cfat.index` and selected using the Pandas `loc` method. Then, following the algorithm of ISLR Algorithm 6.1 *Best Subset Selection*, all $p \\choose k$ models with $k$ predictors were evaluated on the training data and the model returning the best $\\textit{Adjusted}~R^2$ was selected. These models are indicated by the data points for the solid blue line. As the text indicates, other measures (AIC, BIC, $C_p$) would be better than the $\\textit{Adjusted}~R^2$, but we use it becuase because you've already seen the $R^2$ and should have an understanding of what it means.\n",
    "\n",
    "Then, the best models for each $k$ were evaluated for the `test_fat` data. These results are shown as the red dots below the blue line. Note that because the test and train datasets are randomly selected subsets, the results vary from run-to-run and it may that your test data produces better $R^2$ than your training data.\n",
    "\n",
    "This splitting of the data into a test and train set is similar to the *validation set approach* that you can read about in Chapter 5 of ISLR if you choose, but differs in that the two sets may contain duplicate data. The chief reason to use a test and training set is to drill into your skull the limitation of models on unseen data.\n",
    "\n",
    "In the following exercises, you can not use the `Density`, `Fat` or `BMI` columns in your predictive models. You can only use the 13 predictors in the `allowed_factors` list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "allowed_factors = ['Age', 'Weight', 'Height', 'Neck', 'Chest',\n",
    "       'Abdomen', 'Hip', 'Thigh', 'Knee', 'Ankle', 'Biceps', 'Forearm',\n",
    "       'Wrist']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Forward Stepwise Refinement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will manuall perform the steps of the *forward stepwise selection* method for four parameters. You will do this following Algorithm 6.2 from ISLR. For $k = 1\\ldots 4$:\n",
    "* Set up a regression model with $k$ factors that involves the fixed predictors from the previous step $k-1$\n",
    "* Try all $p$ predictors in the new $k$th position\n",
    "* Select the best parameter using $\\textit{Adjusted}-R^2$ (e.g. `model.rsquared_adj`) given your training data\n",
    "* Fix the new parameter and continue the process for $k+1$\n",
    "\n",
    "Then, you will construct a plot similar to the one above, plotting the $\\textit{Adjusted}-R^2$ for each of your $k$ steps and plotting the $\\textit{Adjusted}-R^2$ from the test set using that model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### c. \n",
    "First, construct your training and test sets. These should each contain 200 (possibly duplicated) samples from your `cfat` dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-e4838ff8bbd057fb",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### d. Conduct the algorithm above for $k=1$, leaving your best solution as the answer "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c542ad862ad3b1f5",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### e. Conduct the algorithm above for $k=2$, leaving your best solution as the answer "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-dc5601cbc7971607",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### f. Conduct the algorithm above for $k=3$, leaving your best solution as the answer "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-8434e34c17ce2770",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### g. Conduct the algorithm above for $k=4$, leaving your best solution as the answer "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-40ee9bbaf801529a",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### h. Conduct the algorithm above for $k=5$, leaving your best solution as the answer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-ac9188e2d358271a",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### i. Plot \n",
    "\n",
    "Plot your resulting $\\textit{adjusted}~R^2$ *vs* number of predictors and overlay the $\\textit{adjusted}~R^2$ for the test data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-e20802d76918cedc",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### j. Discussion\n",
    "\n",
    "The BMI model has the benefit being simple (two measurements, height and wright). Looking at your resulting regression model, how many parameters would you suggest to use for your enhanced BMI model? Justify your answer using your models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-9bf6a19839d30e80",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# your answer"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernel_info": {
   "name": "python3"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "nteract": {
   "version": "0.15.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
