# Linear regression/Logistic regression, basic ML 
These are some textbooks including material for linear regression, logistic regression, and basic ML methods.    
- "Introduction to Statistical Learning" (ISBN-13: 978-1461471370). This text is available as a PDF from the [author's site](http://faculty.marshall.usc.edu/gareth-james/ISL/)
- "OpenIntro Statistics". This is an online book. You can download from [here](https://www.openintro.org/stat/textbook.php?stat_book=os).

*more material list will be updated in the future*
