# Project Guide

This class involves a cumulative project where you are required to:    

1. Identify a machine learning problem.
2. Gather data source(s), determine the method of data collection and provenance of the data.
3. Clean the data, reporting on what data was removed, augmented or modified from the data set, why and how it was treated.
4. Possibly transform the data, including adding factors that are combinations of other factors (e.g. perhaps involving computing state-wide summaries from a county-by-county table)
5. Perform analysis using one or several machine learning models.
6. Present the result to the class.
7. Deliverables
**[Deliverable 1]** Jupyter notebook showing a brief problem description, EDA procedure, analysis (model building and training), result and discussion/conclusion.       
**[Deliverable 2]** In-class presentation (please also send me a copy of the slides- pptx or a pdf printout would work)    
**[Deliverable 3]** Create a Git repository with your work and post there (please also include the git repo url in your notebook and/or slides).

Here is the rough timeline for the next 4 weeks:         

1. **Project week 1** In the earlier phase, you were to make the initial selection of a data source and problem to evaluate and discuss it on Piazza. By now, you should know whether you are conducting a regression study or a classification study. You should also indicate in your writeup if you are conducting an inference or prediction study. In this stage, you're going to go through the initial data cleaning (step 3) and transformation (step 4). To do this, you should conduct an exploratory data analysis. If your dataset is not too big (e.g. less than a 100,000 samples), it will probably suffice to read the data into a Pandas data frame and describe aspects of the data.          

2. **Project week 2** Continue more EDA if needed. Start the main analysis (main analysis refers to *machine learning* such as classification or regression, prediction or inference etc OR *other stat analysis*).You are on the right track if you start the main analysis at the latest end of this week or earlier. Depending on your tasks, you may have one model or more. Generally, it is deemed to be a higher quality project if you compare multiple models and show your understanding of why certain model works better than the other or what limitations or cautions certain models may have (and for machine learning models, show enough effort on the hyperparameter optimization).

3. **Project week 3** Continue more main analysis. Hyperparameter optimization. Compare results from your models. Wrap up.

4. **Project week 4** Wrap up and finalize your jupyter notebook write-up. Prepare the presentation. Organize your git repository. Submit the 3 deliverables (finished jupyter notebook write-up, slides, with a link to your project git repository). There will be a submission box in the gradescope. Please upload the jupyter notebook to both gradescope and git repo (the reason why I still need a submission through gradescope is it's easier for me to grade via gradescope). Slides need to be uploaded to gradescope (it only accepts pdf, so you can printout to pdf), and whether you post in git or not is up to you. In the jupyter notebook or slides, please include the url for your git.  
Your github repo must be public and accessible. You do not need to upload data file there (description and some pandas dataframe of data in the write-up is enough). Usually good idea not to upload any large size file in the git.

### EDA procedure (for the Project Week 1 & 2)
- Describe the data sources, the hypothesis or problem you which to analyze and then describe the factors or components that make up the dataset (The "factors" here is called "features" in the machine learning term. These factors are often columns in the tabulated data). For each factor, use a box-plot, scatter plot, histogram etc to describe the distribution of the data as appropriate.
- Describe correlations between different factors of the dataset and justify your assumption that they are correlated or not correlated. You may use numeric or qualitative / graphical analysis for this step.
- Determine if any data needs to be transformed. For example, if you're planning on using a SVM method for prediction, you may need to normalize or scale the data if there is considerable difference in the range of the data.
- Using your hypothesis, indicate if it's likely that you should transform data, such as using a log transform or other transformation of the dataset.
- You should determine if your data has outliers or needs to be cleaned in any way. Are there missing data values for specific factors? How will you handle the data cleaning? Will you discard, interpolate or otherwise substitute data values?
- If you believe that specific factors will be more important than others in your analysis, you should mention which and why. You will use this to confirm your intuitions in your final writeup.

### How to find data
There are a plethora of data resource these days. Here are a few notable (classic ML data) ones.     
- [UCI ML data repository](https://archive.ics.uci.edu/ml/datasets.php): Their data is from researchers mostly and is relatively clean. Also the task types are mostly for classification or regression, therefore many of them are suitable for the scope of this course's project.     
- [Kaggle](https://www.kaggle.com/): Perhaps one of the most popular data science/ML data repositories today, they have many interesting con-going or past competitions. But most of recent dataset/tasks will be beyond the scope of this course. Should you be still interested, choose problems that have tabulated data and classification or regression type tasks.     
- [Data.gov](https://www.data.gov/) has many government sources of data. You can filter for a specific topic (e.g. Finance) and then restrict your attention to e.g. CSV data, which should be easier to process.

Some internet articles about list of dataset:     
- https://medium.com/towards-artificial-intelligence/the-50-best-public-datasets-for-machine-learning-d80e9f030279
- https://towardsdatascience.com/top-sources-for-machine-learning-datasets-bb6d0dc3378b
- https://en.wikipedia.org/wiki/List_of_datasets_for_machine-learning_research
- http://www.statsmodels.org/devel/datasets/index.html
